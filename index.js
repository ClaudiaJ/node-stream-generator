'use strict';
const stream = require('stream');
const generator = Symbol('generator');

function isGenerator(obj) {
    return typeof obj.next === 'function'
        && typeof obj[Symbol.iterator] === 'function';
}

function isGeneratorFunction(obj) {
    var constructor = obj.constructor;
    if (!constructor) return false;
    if (constructor.name === 'GeneratorFunction'
        || constructor.displayName === 'GeneratorFunction')
        return true;
}

class StreamGenerator extends stream.Readable {
    constructor(fn, options) {
        super(Object.assign({}, options, {
            objectMode: true
        }));
        if (isGenerator(fn))
            this[generator] = fn;
        else if (isGeneratorFunction(fn))
            this[generator] = fn();
        else throw new TypeError('fn must be Generator, GeneratorFunction or Iterable object.')
    }
    _read(size) {
        do {
            var {value, done} = this[generator].next();
            if (!value && done) break;
        } while (this.push(value) && !done);

        if (done)
            this.push(null);
    }
}

module.exports = StreamGenerator;
